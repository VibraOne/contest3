Pour le site de mon Portfolio je suis parti sur une simple base qui afficherait ma description, mes projets passés.

Pour cela, j'ai d'abord commencé par un header qui affiche le titre, en fond bleu et bords noirs avec la police "Goblin One".

J'ai aussi pris une image bleue pour le fond du site pour que ça colle avec le reste du site.

La prochaine partie est juste une rapide présentation avec ma photo en position relative, puis mon nom et mon prénom et ma passion "développeur web" avec une animation "keyframe" slidein et la police d'écriture "Noto sans TC", il y a aussi un lien pour accéder à mon CV.

Ensuite nous avons une présentation de moi-même en position absolue, le titre en police "Oswald", puis le texte en dessous en "Noto sans TC".

La liste de mes projets et le texte en dessous arrivent sur la page avec une transition.

Pour finir, la partie contact présente mon adresse mail, mon numéro de téléphone puis des liens vers mon linkedin, instagram et facebook.